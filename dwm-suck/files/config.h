/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 0;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
	"mononoki Nerd Font Mono:style=bold:size=10:antialias=true:autohint=true",
	"SourceCodePro:style=bold:size=10:antialias=true:autohint=true",
};	
static const char dmenufont[]       = "MesloLGS NF:size=12";
static const char col_gray1[]       = "#171717";
static const char col_gray2[]       = "#040405";
static const char col_gray3[]       = "#676C76";
static const char col_gray4[]       = "#494949";
static const char col_cyan[]        = "#040405";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_gray3 },
};

/* tagging */
static const char *tags[] = { "I", "II", "III", "IV", "V" }; 
/* static const char *tags[] = { "", "", "", "", "" };  */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     			instance    	title       tags mask     isfloating   monitor */
	{ "URxvt",     			"rxvt",   	NULL,       0,            0,           -1 },
	{ "URxvt",     			"tg",   	NULL,       0,            0,           -1 },
	{ "URxvt",     			"term", 	NULL,       0,            1,           -1 },
	{ "URxvt",     			"float", 	NULL,       0,            1,           -1 },
	{ "URxvt",     			"pulsemixer",   NULL,       0,            1,           -1 },
	{ "qutebrowser",        	"qutebrowser",	NULL,	    1 << 2,	  0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]",      tile },    /* first entry is default */
	{ "[]",      NULL },    /* no layout function means floating behavior */
	{ "[]",      monocle },
};


/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */

/* dmenu */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-x", "800", "-y", "20", "-z", "300", "-p", "VisoneRun:",  NULL }; 
static const char *launchercmd[] = {"launcher", NULL };

/* the st/urxvt terminal*/
static const char *scratchpadcmd[] = {"sctpad", "float", NULL }; 
static const char *termcmd[]  = {"sctpad", "scratchpad",  NULL };
static const char *urxcmd[] = {"sctpad", "rxvt", NULL };
static const char *urxpmcmd[] = {"sctpad", "pulsemixer", NULL };
static const char *tgcmd[] = {"sctpad", "tg", NULL };

/* Apps bindings */
static const char *qutebcmd[]          = { "sctpad", "qutebrowser", NULL }; 


/* Audio */
#include<X11/XF86keysym.h>

static const char *upvolcmd[]    = { "dwm-vol", "+", NULL };
static const char *downvolcmd[]  = { "dwm-vol", "-", NULL };
static const char *mutevolcmd[]  = { "dwm-vol", "m", NULL };



/* Visone Scripts*/
static const char *sessioncmd[]  = { "void-session", NULL };
static const char *fzfilmscmd[]  = { "term-launcher", "-f", NULL };
static const char *fztvshowcmd[] = { "term-launcher", "-t", NULL };
static const char *fzvarioscmd[] = { "term-launcher", "-v", NULL };
static const char *ssmcmd[]       = { "dwm-screenshoots", "-m", NULL };
static const char *sscmd[]       = { "dwm-screenshoots", "-s", NULL };


#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } }, // dmenu
	{ Mod1Mask,             	XK_d, 	   spawn,  	   {.v = launchercmd } }, // launcher
	{ MODKEY,            	        XK_Return, spawn, 	   {.v = urxcmd } }, // urxvtc st/nnn
	{ MODKEY|ShiftMask,            	XK_Return, spawn,  	   {.v = termcmd } }, // st/nnn tabbed
	{ MODKEY,            		XK_p, 	   spawn,  	   {.v = urxpmcmd } }, // urxvtc pulsemixer
	{ MODKEY,            		XK_t, 	   spawn,  	   {.v = tgcmd } }, // urxvtc tg
	{ Mod1Mask,             	XK_Return, spawn,  	   {.v = scratchpadcmd } }, // urxvtc float
	{ MODKEY,  	      		XK_b,      spawn,  	   {.v = qutebcmd } }, //qute browser
	{ 0,                  		XK_Print,  spawn,          {.v = sscmd } }, //desktop-screenshot
	{ MODKEY,             		XK_Print,  spawn,          {.v = ssmcmd } }, //menu-screenshot
	{ Mod1Mask,           		XK_s,      spawn,          {.v = sessioncmd } }, //session

		/* Audio Bindings */

	{ 0, XF86XK_AudioMute,        	spawn,      {.v = mutevolcmd } }, // audio mute
	{ 0, XF86XK_AudioLowerVolume,   spawn,      {.v = downvolcmd } }, // audio down
	{ 0, XF86XK_AudioRaiseVolume,   spawn,      {.v = upvolcmd   } }, // audio plus
	

		/* Scripts Alt + Key */

	{Mod1Mask,			XK_1,	spawn,		{.v = fzfilmscmd   } },		
	{Mod1Mask,			XK_2,   spawn,		{.v = fztvshowcmd   } },		
	{Mod1Mask,			XK_3,   spawn,		{.v = fzvarioscmd   } },		
	{ MODKEY,				XK_z,         	togglebar,      {0} },
	{ MODKEY,               		XK_Right,     	focusstack,     {.i = +1 } },
	{ MODKEY,               		XK_Left,      	focusstack,     {.i = -1 } },
	{ MODKEY,               		XK_Up,        	incnmaster,     {.i = +1 } },
	{ MODKEY,               		XK_Down,      	incnmaster,     {.i = -1 } },
	{ MODKEY,               		XK_h,         	setmfact,       {.f = -0.05} },
	{ MODKEY,               		XK_l,         	setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,   		XK_Right,     	movestack,      {.i = +1 } },
	{ MODKEY|ControlMask,   		XK_Left,      	movestack,      {.i = -1 } },
	{ MODKEY|ControlMask,   		XK_0,         	zoom,           {0} },
	{ Mod1Mask,               		XK_space,     	togglefloating, {0} },
	{ MODKEY,               		XK_Tab,       	view,           {0} },
	{ MODKEY,               		XK_q,         	killclient,     {0} },
	{ ControlMask,          		XK_1,         	setlayout,      {.v = &layouts[0]} },
	{ ControlMask,          		XK_2,         	setlayout,      {.v = &layouts[1]} },
	{ ControlMask,          		XK_3,         	setlayout,      {.v = &layouts[2]} },
	{ MODKEY,               		XK_0,         	setlayout,      {0} },
	{ MODKEY|ShiftMask,     		XK_F2,        	tag,            {.ui = ~0 } },
	{ MODKEY,               		XK_comma,     	focusmon,       {.i = -1 } },
	{ MODKEY,               		XK_period,    	focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,     		XK_comma,     	tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,     		XK_period,    	tagmon,         {.i = +1 } },
	{ ControlMask,                     	XK_Right,  	viewnext,       {0} },
	{ ControlMask,                       	XK_Left,   	viewprev,       {0} },
	{ ControlMask,             		XK_Up,  	tagtonext,      {0} },
	{ ControlMask,             		XK_Down,   	tagtoprev,      {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        resizemouse,    {0} },
	{ ClkClientWin,         MODKEY,         Button3,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

