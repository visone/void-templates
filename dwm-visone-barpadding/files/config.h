/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const int startwithgaps[]    = { 1 };	/* 1 means gaps are used by default, this can be customized for each tag */
static const unsigned int gappx[]   = { 5 };   /* default gap between windows in pixels, this can be customized for each tag */
static const unsigned int snap      = 35;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int vertpad            = 10;       /* vertical padding of bar */
static const int sidepad            = 70;       /* horizontal padding of bar */
static const char *fonts[]          = {
	"SourceCodePro:style=bold:size=10:antialias=true:autohint=true",
	 "Font Awesome 5 Free:size=10:style=solid:antialias=true:autohint:true"
};	
static const char dmenufont[]       = "SourceCodePro:size=12";
static const char col_gray1[]       = "#0A0A0A";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#8A8A8A";
static const char col_gray4[]       = "#0A0A0A";
static const char col_cyan[]        = "#D9D9D9";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
/*   static const char *tags[] = { "I", "II", "III", "IV", "V" }; */
static const char *tags[] = { "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      		instance   	 title       	tags mask     isfloating   monitor    scratch key */
	{ NULL,			NULL,	    	"scratchpad",		0,	      0,	-1,	      's' },
	{ "Transmission-gtk",	NULL,	    	NULL,	   	   1 << 4,	      0,	-1,	      'r' },
	{ "tabbed",	        "tabbed",   	NULL,	        	0,	      1,	-1,	      't' },
	{ "URxvt",		"urxvt",       	NULL,	        	0,	      1,        -1,	      '2' }, 
	{ "URxvt",		NULL,          "term",	        	0,	      1,        -1,	      'x' }, 
	{ "URxvt",		NULL,          "pulsemixer",	       	0,	      1,        -1,	      'p' }, 
	{ "st",			"st",       	NULL,	        	0,	      1,        -1,	      '1' }, 
	{ "st",			NULL,       	"tg",	    	   1 << 1,	      1,        -1,	      'z' }, 
	{ "st",			NULL,       	"pulsemixer",      	0,	      1,        -1,	      '3' },
	{ "qutebrowser",        "qutebrowser",	NULL,	           1 << 2,	      0,        -1,	      'q' },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 2;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]",      tile },    /* first entry is default */
	{ "[]",      NULL },    /* no layout function means floating behavior */
	{ "[]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
/*First arg only serves to match against key in rules*/
/* dmenu / rofi */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-X", "560", "-Y", "20", "-W", "700", "-p", "VisoneRun:",  NULL }; 
/* static const char *dmenucmd[] = { "vdmenu",  NULL }; */

/* the st/termite terminal*/
static const char *scratchpadcmd[] = {"1", "st-launcher", "-s", "-t", "scratchpad", NULL}; 
static const char *nnncmd[] = {"s", "st-launcher", "-n", "-t", "scratchpad", NULL}; 
static const char *pulsemixercmd[] = {"3", "st-launcher", "-p", "-t", "pulsemixer", NULL}; 
static const char *termcmd[]  = {"t", "vi-tabbed", "-n", "-t", "scratchpad",  NULL };
static const char *urxcmd[] = {"2", "urxvtc", NULL};
static const char *urxtermcmd[] = {"x", "term-launcher", "-s", "-t", "term", NULL};
static const char *urxpulsemixercmd[] = {"p", "term-launcher", "-p", "-t", "pulsemixer", NULL};

/* Apps bindings */
static const char *tlgcmd[]        = {"z", "st", "-e", "tg", "-t", "tg", NULL };
static const char *qutebcmd[]          = { "q", "qutebrowser", NULL};


/* Audio */
#include<X11/XF86keysym.h>

static const char *upvolcmd[]    = { "dwm-vol", "+",     NULL };
static const char *downvolcmd[]  = { "dwm-vol", "-",    NULL };
static const char *mutevolcmd[]  = { "dwm-vol", "m", NULL };

/* Visone Scripts*/
static const char *sessioncmd[]  = { "void-session", NULL };
static const char *filmscmd[] 	 = { "films", NULL };
static const char *tvshowcmd[] 	 = { "tvshow", NULL };
static const char *varioscmd[] 	 = { "varios", NULL };
static const char *selektahcmd[] = { "selektah", NULL };
static const char *rdeskcmd[] 	 = { "rec-dsktp", NULL };
static const char *ssmcmd[]       = { "dwm-screenshoots", "-m", NULL };
static const char *sscmd[]       = { "dwm-screenshoots", "-s", NULL };
static const char *upcmd[]       = { "void-updates", NULL };


#include "movestack.c"
static Key keys[] = {
	/* Visone Bindings */
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } }, // dmenu
	{ MODKEY,            	        XK_Return, togglescratch,  {.v = termcmd } }, // tabbed st/nnn
	{ MODKEY|ShiftMask,            	XK_Return, togglescratch,  {.v = urxcmd } }, // urxvtc tabbed
	{ MODKEY|ShiftMask,            	XK_p,      togglescratch,  {.v = urxpulsemixercmd } }, // urxvtc pulsemixer
	{ MODKEY|ShiftMask,            	XK_n,      togglescratch,  {.v = urxtermcmd } }, // urxvtc term
	{ Mod1Mask,             	XK_Return, togglescratch,  {.v = scratchpadcmd } }, // st
	{ MODKEY,             		XK_p,      togglescratch,  {.v = pulsemixercmd } }, // pulsemixer
	{ MODKEY,             		XK_n,      togglescratch,  {.v = nnncmd } }, // nnn
	{ MODKEY,  	      		XK_b,      togglescratch,  {.v = qutebcmd } }, //qutebrowser
	{ MODKEY,	      		XK_t,      togglescratch,  {.v = tlgcmd } }, // telegram
	{ 0,                  		XK_Print,  spawn,          {.v = sscmd } }, //desktop-screenshot
	{ MODKEY,             		XK_Print,  spawn,          {.v = ssmcmd } }, //menu-screenshot
	{ Mod1Mask,           		XK_s,      spawn,          {.v = sessioncmd } }, //session
	{ MODKEY,             		XK_u,      spawn,          {.v = upcmd } }, // void-updates

		/* Audio Bindings */

	{ 0, XF86XK_AudioMute,        	spawn,      {.v = mutevolcmd } }, // audio mute
	{ 0, XF86XK_AudioLowerVolume,   spawn,      {.v = downvolcmd } }, // audio down
	{ 0, XF86XK_AudioRaiseVolume,   spawn,      {.v = upvolcmd   } }, // audio plus
	

		/* Scripts Alt + Key */

	{Mod1Mask,			XK_1,	spawn,		{.v = filmscmd   } },		
	{Mod1Mask,			XK_2,   spawn,		{.v = tvshowcmd   } },		
	{Mod1Mask,			XK_3,   spawn,		{.v = varioscmd   } },		
	{Mod1Mask,			XK_4,	spawn,		{.v = selektahcmd } },	
	{Mod1Mask,			XK_0,	spawn,		{.v = rdeskcmd   } },		


	/* DWM bindings */
	{ MODKEY,				XK_z,         togglebar,      {0} },
	{ MODKEY,               		XK_Right,     focusstack,     {.i = +1 } },
	{ MODKEY,               		XK_Left,      focusstack,     {.i = -1 } },
	{ MODKEY,               		XK_Up,        incnmaster,     {.i = +1 } },
	{ MODKEY,              			XK_Down,      incnmaster,     {.i = -1 } },
	{ MODKEY,               		XK_h,         setmfact,       {.f = -0.05} },
	{ MODKEY,               		XK_l,         setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,   		XK_Right,     movestack,      {.i = +1 } },
	{ MODKEY|ControlMask,   		XK_Left,      movestack,      {.i = -1 } },
	{ MODKEY|ControlMask,   		XK_0,         zoom,           {0} },
	{ MODKEY,               		XK_Tab,       view,           {0} },
	{ Mod1Mask,               		XK_space,     togglefloating, {0} },
	{ Mod1Mask,                       	XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ Mod1Mask,                       	XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ Mod1Mask,                       	XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ Mod1Mask,                       	XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ Mod1Mask|ShiftMask,             	XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ Mod1Mask|ShiftMask,             	XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ Mod1Mask|ShiftMask,             	XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ Mod1Mask|ShiftMask,             	XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	{ Mod1Mask|ControlMask,           	XK_Up,     moveresizeedge, {.v = "t"} },
	{ Mod1Mask|ControlMask,           	XK_Down,   moveresizeedge, {.v = "b"} },
	{ Mod1Mask|ControlMask,           	XK_Left,   moveresizeedge, {.v = "l"} },
	{ Mod1Mask|ControlMask,           	XK_Right,  moveresizeedge, {.v = "r"} },
	{ Mod1Mask|ControlMask|ShiftMask, 	XK_Up,     moveresizeedge, {.v = "T"} },
	{ Mod1Mask|ControlMask|ShiftMask, 	XK_Down,   moveresizeedge, {.v = "B"} },
	{ Mod1Mask|ControlMask|ShiftMask, 	XK_Left,   moveresizeedge, {.v = "L"} },
	{ Mod1Mask|ControlMask|ShiftMask, 	XK_Right,  moveresizeedge, {.v = "R"} },
	{ MODKEY,               		XK_Tab,       view,           {0} },
	{ MODKEY,               		XK_q,         killclient,     {0} },
	{ ControlMask,          		XK_1,         setlayout,      {.v = &layouts[0]} },
	{ ControlMask,          		XK_2,         setlayout,      {.v = &layouts[1]} },
	{ ControlMask,          		XK_3,         setlayout,      {.v = &layouts[2]} },
	{ MODKEY,               		XK_0,         setlayout,      {0} },
	{ MODKEY,               		XK_space,     togglefloating, {0} },
	{ MODKEY,               		XK_F1,        view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,     		XK_F2,        tag,            {.ui = ~0 } },
	{ MODKEY,               		XK_comma,     focusmon,       {.i = -1 } },
	{ MODKEY,               		XK_period,    focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,     		XK_comma,     tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,     		XK_period,    tagmon,         {.i = +1 } },
	{ ControlMask,                       	XK_Right,     viewnext,       {0} },
	{ ControlMask,                       	XK_Left,      viewprev,       {0} },
	{ ShiftMask,             		XK_Right,     tagtonext,      {0} },
	{ ShiftMask,             		XK_Left,      tagtoprev,      {0} },
	{ MODKEY,               		XK_minus,     setgaps,        {.i = -5 } },
	{ MODKEY,               		XK_plus,      setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,     		XK_minus,     setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,     		XK_plus,      setgaps,        {.i = GAP_TOGGLE} },
	TAGKEYS(                        	XK_1,                      0)
	TAGKEYS(                        	XK_2,                      1)
	TAGKEYS(                        	XK_3,                      2)
	TAGKEYS(                        	XK_4,                      3)
	TAGKEYS(                        	XK_5,                      4)
	TAGKEYS(                        	XK_6,                      5)
	TAGKEYS(                        	XK_7,                      6)
	TAGKEYS(                        	XK_8,                      7)
	TAGKEYS(                        	XK_9,                      8)
	{ MODKEY|ShiftMask,             	XK_q,      quit,           {1} },
	{ MODKEY|ControlMask|ShiftMask, 	XK_q,      quit,           {0} }, 
};
/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
